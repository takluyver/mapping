import networkx as nx
#from two_way_dict import TwoWayDict

LOAD_TDWG = True

class TwoWayDict(dict):
    def get_key(self, value):
        for k, v in self.items():
            if v == value:
                return k
        raise ValueError(value)

class Region(object):
    def __init__(self, code, inmap):
        self.code = code
        self._inmap = inmap
        
    def __repr__(self):
        name = self._inmap.nameindex.get_key(self.code)
        if name:
            return "<Region: {0} ~ {1}, {2} children>"\
                    .format(self.code, name, len(self.children())) 
        return "<Region: {0}, {1} children>"\
                    .format(self.code, len(self.children()))
        
    def children(self):
        return self._inmap._map.successors(self.code)
        
    def parents(self):
        return self._inmap._map.predecessors(self.code)
        
    def containsregion(self, code):
        if code == self.code:
            return True
        return any(self._inmap[child].containsregion(code)\
                    for child in self.children())
  
class _NumCode(int):
    """A meaningless but hashable numeric code."""
    def __repr__(self):
        return "[UR{0}]".format(self)
        
    def __hash__(self):
        return hash(self.__class__) ^ int.__hash__(self)
        
    def __eq__(self, other):
        return isinstance(other, _NumCode) and int(self) == int(other)
    def __ne__(self, other):
        return not(self == other)

class Map(object):
    """A map is a collection of regions, which keeps track of relationships,
    such as 'The Netherlands is in Middle Europe':
    worldmap.byname("Middle Europe").containsregion("NET")  # --> True
    """
    def __init__(self, rootname="!!WORLD"):
        self.rootname = rootname
        self.nameindex = TwoWayDict()
        self._map = nx.DiGraph()
        self.indices = {}
        self._next_unused_code_no = 0
        
    def _get_unused_code(self):
        """Get a meaningless numeric code for a region. It is unique when created,
        but may be overwritten later."""
        self._next_unused_code_no += 1
        return _NumCode(self._next_unused_code_no)
        
    def add_region(self, code=None, name=None, parent=None, children=[], **indexrefs):
        """Add a region to the map, by code and  an optional name. If you don't
        specify a code, a new numeric code will be created for you. The code will
        be returned.
        
        If you specify a parent region (by code), the region will be added
        within that (specify the smallest region which contains it entirely).
        If not, it will be a floating region.
        
        To build more complex maps (i.e. not a strict hierarchy), you may also
        pass a list of codes to become children of this region.
        
        You can add a series of index=reference pairs, to add the name to
        different indices.
        
        E.g.
        worldmap.add_region("NET", "Netherlands", "11", ISO="NL")
        MUTcode = worldmap.add_region(name="Madeupitania",
                                        children=["MUT-N","MUT-C","MUT-S"])
        """
        if not code:
            code = self._get_unused_code()
        self._map.add_node(code)
        
        if parent:
            self._map.add_edge(parent, code)
        for child in children:
            self._map.add_edge(code, child)
        
        if name:
            self.nameindex[name] = code
        for index, ref in indexrefs.items():
            if ref:
                self.indices[index][ref] = code
        
        return code
                
    def children(self):
        """Get first level regions in the map."""
        return self._map.successors(self.rootname)
                
    def __getitem__(self, code):
        return Region(code, self)
        
    def byname(self, name):
        """Get a region by its name in the nameindex."""
        return self[self.nameindex[name]]
        
    def __repr__(self):
        return "<Map, with {0} top level regions>"\
                    .format(len(self.children()))
    
    def __len__(self):
        return len(self._map)

if LOAD_TDWG:
    import tdwg_data
    world = Map()
    world.indices["ISO"] = {}

    for code, name, ISO, parent, notes in tdwg_data.data:
        if name in world.nameindex:
            name = None
        if ISO in world.indices["ISO"]:
            ISO = None
        world.add_region(code, name, parent, ISO=ISO)
        
    for name, parent, children in tdwg_data.extra_countries:
        world.add_region(name=name, parent=parent, children=children)
    
    for name, refname in tdwg_data.extra_names.items():
        world.nameindex[name] = world.nameindex[refname]
        
    for ISO, name in tdwg_data.extra_ISO.items():
        world.indices["ISO"][ISO] = world.nameindex[name]
        
    tdwg_levels = tdwg_data.tdwg_by_level   
