#!/usr/bin/env python
###############################################################################
# two_way_dict python module
# Copyright (c) 2005-2008 RADLogic Pty. Ltd. 
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above
#      copyright notice, this list of conditions and the following
#      disclaimer in the documentation and/or other materials provided
#      with the distribution.
#    * Neither the name of RADLogic nor the names of its contributors
#      may be used to endorse or promote products derived from this
#      software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
###############################################################################
"""Provide dictionary-style object with reverse-lookup capabilities.

Running this module as a script will run the unittest suite.

This requires Python 2.2 or greater.

"""

__author__ = 'Tim Wegener <twegener@radlogic.com.au>'
__version__ = '$Revision: 0.3 $'
__date__ = '$Date: 2008/07/31 04:05:17 $'
__credits__ = '''\
Stuart Mentzer <Stuart_Mentzer - objexx.com> - suggestions/bugfix for replace
'''

# $Log: two_way_dict.py,v $
# Revision 0.3  2008/07/31 04:05:17  twegener
# Delete old value in _reverse_map when replacing an existing key, reported by
# Stuart Mentzer <Stuart_Mentzer - objexx.com> - suggestions/bugfix for replace.
# Changed behavior of setitem when the value already exists, to be more
# symmetrical, also at Stuart's suggestion.
# Replaced some 'has_key' method calls with 'in'.
# Added todo item to replace UnitTest test suite with doctests.
#

import unittest


class TwoWayDict(dict):
    """Dictionary with reverse mapping.

    This class maintains the integrity of the two-way mapping.

    This acts just like a normal dict, but has extra methods such as:
    key(value)
    get_key(value)
    has_value(value)
    pop_key(value, [default])
    reversed_items()
    reversed_iteritems()
    reversed_popitem()

    Of these, key, get_key, has_value and reversed_items are the more commonly
    used methods.

    Notes:
    - Both keys and values must be hashable.
    - Many to one mappings are allowed, but only the first reverse mapping will be
      found.
    - This takes double the storage of a normal dict.
      (Plus a little overhead.)
    - x in <TwoWayDict instance> will only return True if x is a forward key
    - Reverse lookups are implemented using a second dictionary,
      so has_value, key and get_key are O(1) operations.

    """

    def __init__(self, *args, **kwargs):

        # The user should not tamper with the internal reverse map,
        # since this enables the integrity of the TwoMayDict to be messed up.
        # Signify this with a leading underscore.
        self._reverse_map = {}

        dict.__init__(self)

        # It appears that dict.__init__ does not call self.update,
        # so need to do that here.
        self.update(*args, **kwargs)

    def __setitem__(self, k, value):
        """x.__setitem__(i, y) <==> x[i]=y"""
        
        # Check for 1-to-many mappings.
        #if value in self._reverse_map:
        #    del self[self._reverse_map[value]]

        if k in self:
            del self._reverse_map[self[k]]
        
        if not value in self._reverse_map:
            self._reverse_map[value] = k
        
        dict.__setitem__(self, k, value)

    def __delitem__(self, k):
        """x.__delitem__(y) <==> del x[y]"""

        value = self[k]
        del self._reverse_map[value]
        dict.__delitem__(self, k)
        for newk, matchv in self.items():
            if matchv == value:
                self._reverse_map[value] = newk
                break

    def __repr__(self):
        """x.__repr__() <==> repr(x)"""

        return "%s(%s)" % (self.__class__.__name__, dict.__repr__(self))

    def copy(self):
        """Return a shallow copy."""

        return self.__class__(self)

    def clear(self):
        """Remove all items."""

        dict.clear(self)
        self._reverse_map.clear()

    def key(self, value):
        """Get key corresponding to value.

        Raise KeyError if value is not present.

        """
        return self._reverse_map[value]

    def get_key(self, value, default=None):
        """Get key corresponding to value, or else a default.

        Return default if value not present.

        """
        return self._reverse_map.get(value, default)

    def has_value(self, value):
        """Return True if value is present."""

        return value in self._reverse_map

    def reversed_items(self):
        """Return a list of (value, key) pairs."""

        return self._reverse_map.items()

    def pop(self, key, *args):
        """Remove specified key and return corresponding value.

        If key is not found, default is returned if given,
        otherwise KeyError is raised.

        """
        # Don't use dict.pop, so that it works with Python 2.2.
        try:
            val = self[key]
        except KeyError:
            if not args:
                raise
            else:
                val = args[0]
        else:
            del self[key]
        return val

    def popitem(self):
        """Return and remove arbitrary (key, value) pair."""

        item = dict.popitem(self)
        del self._reverse_map[item[1]]
        return item

    def pop_key(self, value, *args):
        """Remove specified value and return corresponding key.

        If value is not found, default is returned if given,
        otherwise KeyError is raised.
        
        """
        key = self._reverse_map.pop(value, *args)
        try:
            del self[key]
        except KeyError:
            pass
        return key

    def reversed_popitem(self):
        """Return and remove arbitrary (value, key) pair."""

        item = self._reverse_map.popitem()
        del self[item[0]]
        return item

    def update(self, dict=None, **kwargs):
        """Update from dict, or sequence of key, value pairs or kwargs."""
        
        # dict.update does not seem to call self.__getitem__,
        # so need to redefine update method here.

        # todo: Support sequence of (key, value) pairs here.
        if dict is not None:
            for k, v in dict.items():
                self[k] = v

        for k, v in kwargs.items():
            self[k] = v

    def fromkeys(cls, iterable, value=None):
        """Return new TwoWayDict with keys from iterable and values set to v.

        value defaults to None.

        This isn't very useful due to the one-to-one mapping restriction,
        but here to match Py2.3/2.4 dict interface.
        
        """
        # It is redefined here, so that it works in Py2.2.

        twd = cls()
        for key in iterable:
            twd[key] = value
        return twd
    fromkeys = classmethod(fromkeys)

    def fromvalues(cls, iterable, key=None):
        """Return new TwoWayDict with values from iterable and keys set to key.

        key defaults to None.

        This isn't very useful due to the one-to-one mapping restriction,
        but here for symmetry.
        
        """
        d = cls()
        for value in iterable:
            d[key] = value
        return d
    fromvalues = classmethod(fromvalues)

